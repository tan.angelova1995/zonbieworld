package com.example.zonbieworld;

import com.example.zonbieworld.model.UnitHeight;
import com.example.zonbieworld.model.UnitWeight;
import com.example.zonbieworld.model.User;
import com.example.zonbieworld.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ZonbieworldApplicationTests {

    @Autowired
    UserService userService;

    @Test
    public void testName(){
        User user1 = new User();
        user1.setFirstName("Tanya");
        user1.setLastName("Angelova");
        user1.setEmail("tan.angelova1995@gmail.com");
      //  user1.setPassword("secret");
     //    user1.setHeight(158.0, UnitHeight.cm);
     //   user1.setWeight(58.5, UnitWeight.kg);
      //  user1.setActivityLevel(0.8);
      //  user1.setSex("Female");
       // user1.setAge(26);
        Long id = userService.createUser(user1);
        assertEquals("Tanya Angelova", userService.getUserById(id).getName());
    }

}
