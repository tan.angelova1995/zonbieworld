package com.example.zonbieworld.controller;

import com.example.zonbieworld.dto.RecipeDTO;
import com.example.zonbieworld.model.Recipe;
import com.example.zonbieworld.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @GetMapping("/recipes")
    public List<RecipeDTO> getAllRecipes() {
        return recipeService.getAllRecipes();
    }

    @GetMapping("/recipeId")
    public RecipeDTO getRecipeById( @RequestParam(name="id", required = true) Long recipeId){
        return recipeService.getRecipeDTOById(recipeId);
    }

    @DeleteMapping("/delRecipeById")
    public ResponseEntity<Long> delRecipeById(@RequestParam(name="id", required = true) Long recipeId){
        boolean isRemoved = recipeService.deleteRecipeById(recipeId);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(recipeId, HttpStatus.OK);
    }

    @PostMapping("/createRecipe")
    // I have to have json file as input
    public ResponseEntity<Long> createRecipe(@RequestBody Recipe recipe){

       // Recipe recipe = new Recipe("hello");
        Long createdId = recipeService.createRecipe(recipe);

        if (createdId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(createdId, HttpStatus.OK);

    }

    @DeleteMapping("/deleteAllRecipes")
    public ResponseEntity deleteAllRecipes(){
        if (recipeService.deleteAllRecipes()){
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/addIngredientToRecipe")
    public ResponseEntity addIngredientToRecipe(@RequestParam(name="idRecipe", required = true) Long recipeId, @RequestParam(name="idIngredient", required = true) Long ingredientId){
          if  (recipeService.addIngredientToRecipe(recipeId, ingredientId)){
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
