package com.example.zonbieworld.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "foods")
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long foodID;
    @Column(name = "nameFood")
    private String nameFood;
    @Column(name = "typeFood")
    private TypeFood typeFood;
    @Column(name = "blockType")
    private BlockType blockType;
    @Column(name = "foodQuality")
    private FoodQuality foodQuality;
    @Column(name = "quantityPerBlock")
    private Double quantityPerBlock;
    @Column(name = "unitFood")
    private UnitFood unitFood;
}
