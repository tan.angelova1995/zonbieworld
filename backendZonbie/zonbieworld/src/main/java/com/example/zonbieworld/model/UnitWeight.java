package com.example.zonbieworld.model;

public enum UnitWeight {
    kg,
    pounds
}
