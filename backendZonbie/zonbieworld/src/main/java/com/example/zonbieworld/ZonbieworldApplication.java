package com.example.zonbieworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class   ZonbieworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZonbieworldApplication.class, args);
	}

}
