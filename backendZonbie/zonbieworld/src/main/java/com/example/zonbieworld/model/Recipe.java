package com.example.zonbieworld.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "recipes")
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="recipe_id")
    private Long recipeID;
    @Column(name="name")
    private String nameRecipe;
   // @Column(name="ingredients")
    //private Map<Ingredient, Quantity> quantityMap;
   @OneToMany(targetEntity=Ingredient.class,cascade=CascadeType.ALL)//, mappedBy="recipe",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
   private List<Ingredient> ingredients = new ArrayList<>();
    @Column(name="instructions")
    private String instructions;
    @Column(name="fat")
    private Double blocksFat; // total
    @Column(name="protein")
    private Double blocksProtein;// total
    @Column(name="carbs")
    private Double blocksCarbs;// total
    public Recipe(){}
    public Recipe(String name, String instructions, Double blocksFat, Double blocksProtein, Double blocksCarbs){
        this.nameRecipe = name;
        this.instructions = instructions;
        this.blocksFat = blocksFat;
        this.blocksProtein = blocksProtein;
        this.blocksCarbs = blocksCarbs;
    }
    public Long getRecipeId(){
        return this.recipeID;
    }

    public void setNameRecipe(String newName){
        this.nameRecipe = newName;
    }

    public String getNameRecipe(){
        return this.nameRecipe;
    }

    public String getInstructions(){
        return this.instructions;
    }

    public void setInstructions(String instructions){
        this.instructions = instructions;
    }

    public Double getBlocksFat(){
        return this.blocksFat;
    }

    public void setBlocksFat(Double newTotalFat){
        this.blocksFat = newTotalFat;
    }

    public Double getBlocksProtein(){
        return this.blocksProtein;
    }

    public void setBlocksProtein(Double newTotalProtein){
        this.blocksProtein = newTotalProtein;
    }

    public Double getBlocksCarbs(){
        return this.blocksCarbs;
    }

    public void setBlocksCarbs(Double newTotalCarbs){
        this.blocksCarbs = newTotalCarbs;
    }

    public List<Ingredient> getIngredients(){return this.ingredients;}

    public Boolean addIngredient(Ingredient ingredient){
        try {
            // ingredient.add(this);
            this.ingredients.add(ingredient);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public String toString(){
        return this.nameRecipe + " with id " + this.recipeID + " and with instructions: " + this.instructions + " and with ingredients " + this.ingredients;
    }

    public Long getRecipeID() {
        return recipeID;
    }

    public void setRecipeID(Long recipeID) {
        this.recipeID = recipeID;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
