package com.example.zonbieworld.service;

import com.example.zonbieworld.dto.UserDTO;
import com.example.zonbieworld.model.User;
import com.example.zonbieworld.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<UserDTO> getAllUsers(){
        return this.userRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());

    }

    @Override
    public UserDTO convertEntityToDto(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setName(user.getFirstName(), user.getLastName());
        //userDTO.setSex(user.getSex());
        return userDTO;
    }

    @Override
    public UserDTO getUserById(Long userId){
        Optional<User> optinalEntity =  userRepository.findById(userId);
        User userEntity = optinalEntity.get();
        return this.convertEntityToDto(userEntity);
    }

    @Override
    public Boolean deleteUserById(Long userId){
        try {
            Optional<User> optinalEntity = userRepository.findById(userId);
            User userEntity = optinalEntity.get();
            userRepository.delete(userEntity);
            return true;
        } catch (NoSuchElementException e){
            return false;
        }
    }

    @Override
    public Long createUser(@RequestBody User user){
        userRepository.save(user);
        return user.getId();
    }

    public Boolean deleteAllUsers(){
        try{
            userRepository.deleteAll();
            return true;
        } catch (Exception e){
            return false;
        }

    }

    @Override
    public Double getNbProteinBlocks(Long userId){
        User user = userRepository.getById(userId);
        return user.findNbBlocksPerDay();
    }

}
