package com.example.zonbieworld.model;

public enum UnitFood {
        teaspoon,
        tablespoon,
        cup,
        number,
        kg,
        g,
        ml,
        l

}
