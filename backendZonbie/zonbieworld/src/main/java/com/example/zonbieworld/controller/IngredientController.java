package com.example.zonbieworld.controller;

import com.example.zonbieworld.dto.IngredientDTO;
import com.example.zonbieworld.model.Ingredient;
import com.example.zonbieworld.service.IngredientService;
import com.example.zonbieworld.service.RecipeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class IngredientController {


    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private RecipeService recipeService;

    @GetMapping("/ingredients")
    public List<IngredientDTO> getAllRecipes() {
        return ingredientService.getAllIngredients();
    }

    @GetMapping("/ingredientId")
    public IngredientDTO getRecipeById( @RequestParam(name="id") Long ingredientId){
        return ingredientService.getIngredientById(ingredientId);
    }

    @DeleteMapping("/delIngredientById")
    public ResponseEntity<Long> delIngredientById(@RequestParam(name="id") Long ingredientId){
        boolean isRemoved = ingredientService.deleteIngredientById(ingredientId);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(ingredientId, HttpStatus.OK);
    }

    @PostMapping("/createIngredient")
    // I have to have json file as input
    public ResponseEntity<Long> createIngredient(@Valid @RequestBody Ingredient ingredient){//@RequestParam(value = "recipeId") Long recipeID, @Valid @RequestBody Ingredient ingredient){
      //  Recipe recipe = recipeService.getRecipeById(recipeID);
       // ingredient.setRecipe(recipe);
        Long createdId = ingredientService.createIngredient(ingredient);
        if (createdId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(createdId, HttpStatus.OK);
    }

    @GetMapping("/searchIngredient/{nameIngredient}/{pageSize}")
    public ResponseEntity<String> searchIngredient(@PathVariable("nameIngredient") String nameIngredient, @PathVariable("pageSize") String pageSize) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.nal.usda.gov/fdc/v1/foods/search?query="+nameIngredient.toLowerCase()+"&pageSize="+pageSize+"&api_key=z62sLvzmGjv62Fd0XGUDa5N3W5ctfAMpPzYVgYT2";
        String resp = restTemplate.getForObject(url, String.class);
        System.out.println(resp);
        ObjectMapper mapper = new ObjectMapper();
       // JsonNode jsonNode = mapper.readTree(restTemplate.getForEntity(url).toString());
        //System.out.println(jsonNode);
       /* JsonParser springParser = JsonParserFactory.getJsonParser();
        Map< String, Object > map = springParser.parseMap(resp);*/
       ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        //System.out.println(jsonNode.get("foods").get("description"));
       return response;
      //  return ResponseEntity.ok(gson.toJson("This is a String"));
       // return new ResponseEntity(jsonNode.get("foods").get("description"),
        //                HttpStatus.OK);
    }

    @DeleteMapping("/deleteAllIngredients")
    public ResponseEntity deleteAllIngredients(){
        if (ingredientService.deleteAllIngredients()){
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
