package com.example.zonbieworld.model;

public enum FoodQuality {
    poor,
    fair,
    best
}
