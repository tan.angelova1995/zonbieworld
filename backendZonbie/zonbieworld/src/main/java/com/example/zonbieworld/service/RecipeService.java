package com.example.zonbieworld.service;

import com.example.zonbieworld.dto.RecipeDTO;
import com.example.zonbieworld.model.Recipe;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RecipeService {
    public List<RecipeDTO> getAllRecipes();
    public Recipe getRecipeById(Long recipeId);
    public RecipeDTO getRecipeDTOById(Long recipeId);
    public Boolean deleteRecipeById(Long recipeId);
    public Long createRecipe(Recipe recipe);
    public RecipeDTO convertEntityToDto(Recipe recipe);
    public Boolean deleteAllRecipes();
    public Boolean addIngredientToRecipe(Long idRecipe, Long idIngredient);
}
