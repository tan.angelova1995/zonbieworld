package com.example.zonbieworld.service;

import com.example.zonbieworld.dto.IngredientDTO;
import com.example.zonbieworld.model.Ingredient;
import com.example.zonbieworld.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IngredientServiceImpl implements IngredientService{

    @Autowired
    IngredientRepository ingredientRepository;

    @Override
    public List<IngredientDTO> getAllIngredients(){
        return this.ingredientRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public IngredientDTO getIngredientById(Long ingredientId){
        Optional<Ingredient> optionalEntity =  ingredientRepository.findById(ingredientId);
        Ingredient ingredientEntity = null;
        if (optionalEntity.isPresent()){
            ingredientEntity = optionalEntity.get();
        }
        return this.convertEntityToDto(ingredientEntity);
    }

    @Override
    public Boolean deleteIngredientById(Long ingredientId){
        try {
            Optional<Ingredient> optionalEntity = ingredientRepository.findById(ingredientId);
            Ingredient ingredientEntity = optionalEntity.get();
            ingredientRepository.delete(ingredientEntity);
            return true;
        } catch (NoSuchElementException e){
            return false;
        }
    }

    @Override
    public Long createIngredient(@RequestBody Ingredient ingredient){
        ingredientRepository.save(ingredient);
        return ingredient.getIngredientID();
    }

    @Override
    public IngredientDTO convertEntityToDto(Ingredient ingredient){
        IngredientDTO ingredientdto = new IngredientDTO();
        ingredientdto.setIngredientID(ingredient.getIngredientID());
        ingredientdto.setRecipeId(ingredient.getRecipe().getRecipeId());
        return ingredientdto;
    }

    @Override
    public Boolean deleteAllIngredients(){
        try{
            ingredientRepository.deleteAll();
            return true;
        } catch (Exception e){
            return false;
        }
    }
}
