import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.module'

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe("A test recipe", "Some kind of a description", "https://images.freeimages.com/images/small-previews/e7c/recipe-1538714.jpg")
  ];

  constructor() { }

  ngOnInit(): void {
  }

  onNewRecipe() {
    this.recipes.push(new Recipe("New recipe", "Very nice description", "https://images.freeimages.com/images/small-previews/e7c/recipe-1538714.jpg"));
  }

}
