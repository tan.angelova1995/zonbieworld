export class Recipe {
    public name: string;
    public description: string;
    public imagePath: string;
    public dateAdded = new Date();
    public date: number;

    constructor(name: string, desc: string, imagePath: string) {
        this.name = name;
        this.description = desc;
        this.imagePath = imagePath;
        this.date = this.dateAdded.getDate();
    }
}