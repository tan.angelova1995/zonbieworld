import { Component } from "@angular/core";
import { Ingredient } from "../../shared/ingredient.module";

@Component({
    selector: 'app-recipe',
    templateUrl: './recipe.component.html',
    styleUrls: ['./recipe.component.css']
})
export class Recipe {
    picture: string = ""; // we're giving the url of the image
    name: string = "Test name recipe";
    proteinBlocks: number = 0;
    carbohydrateBlocks: number = 0;
    fatBlocks: number = 0;
    ingredients: Ingredient[] = [
        // new Ingredient("Apples"),
        //  new Ingredient("Tomatoes")
    ];
    dateAdded = new Date();

    constructor() {
        this.ingredients.push(new Ingredient("Tomato", 1));
        this.ingredients.push(new Ingredient("Cucumber", 1));
        this.ingredients.push(new Ingredient("Milk", 100));
    }
    addIngredient(ingredient: Ingredient) {
        this.ingredients.push(ingredient);
    }

    displayMeasureIngredient(ingredient: Ingredient) {
        return ingredient.measure !== "num";
    }
    // displayMeasureIngredient() {
    //     return false;
    // }
}