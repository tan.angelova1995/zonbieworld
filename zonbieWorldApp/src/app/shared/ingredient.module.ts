export class Ingredient {
    public name: string = "testName";
    public amount: number = 0;
    measure: string = "nb";
    proteins: number = 0; // hom much of this ingredient is 1 block of proteins
    carbohydrates: number = 0; // hom much of this ingredient is 1 block of carbohydrates
    fat: number = 0; // hom much of this ingredient is 1 block of fat

    constructor(name: string, amount: number) {
        this.name = name;
        this.amount = amount;
    }

}