import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.module';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[] = [
    new Ingredient("tomato", 1),
    new Ingredient("cucumber", 2),
    new Ingredient("Avocado", 1)
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
